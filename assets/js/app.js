// We import the CSS which is extracted to its own file by esbuild.
// Remove this line if you add a your own CSS build pipeline (e.g postcss).
import "../css/app.css"

// If you want to use Phoenix channels, run `mix help phx.gen.channel`
// to get started and then uncomment the line below.
//import "./user_socket.js"

// You can include dependencies in two ways.
//
// The simplest option is to put them in assets/vendor and
// import them using relative paths:
//
//     import "../vendor/some-package.js"
//
// Alternatively, you can `npm install some-package --prefix assets` and import
// them using a path starting with the package name:
//
//     import "some-package"
//

// Include phoenix_html to handle method=PUT/DELETE in forms and buttons.
import "phoenix_html"
// Establish Phoenix Socket and LiveView configuration.
import {Socket} from "phoenix"
import {LiveSocket} from "phoenix_live_view"
import topbar from "../vendor/topbar"

let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content")
let peerConnection = null;
let remoteStream = new MediaStream();
let signalingChannel = null;

function pushMessage(type, content) {
  signalingChannel.push(type, content);
}

function createPeerConnection(stream) {
  let pc = new RTCPeerConnection({
    iceServers: [
      {
        urls: 'stun:stun.l.google.com:19302',
      },
    ],
  });

  pc.ontrack = (event) => {
    console.log('ontrack');
    remoteStream.addTrack(event.track);
    document.getElementById('remote-audio').srcObject = remoteStream
  };

  pc.onicecandidate = (event) => {
    console.log('oncandidate');
    if(event.candidate !== null)
      pushMessage('ice_candidate', {candidate: event.candidate});
  };

  stream.getTracks().forEach(track => pc.addTrack(track));
  return pc;
}

async function call() {
  let offer = await peerConnection.createOffer();
  peerConnection.setLocalDescription(offer);
  pushMessage('sdp_offer', {offer});
}

function receiveRemote(offer) {
  let remoteDescription = new RTCSessionDescription(offer);
  peerConnection.setRemoteDescription(remoteDescription);
}

async function answerCall(offer) {
  receiveRemote(offer);
  let answer = await peerConnection.createAnswer();
  peerConnection
    .setLocalDescription(answer)
    .then(() =>
      pushMessage('sdp_answer', {answer: peerConnection.localDescription})
    );
}

function unsetSource(element) {
  if (element.srcObject) {
    element.srcObject.getTracks().forEach(track => track.stop());
  }
  element.removeAttribute('src');
  element.removeAttribute('srcObject');
}

async function connect(roomId) {
  const localStream = await navigator.mediaDevices.getUserMedia({audio: true, video: false})

  signalingChannel = socket.channel(`room:${roomId}`, {})
  signalingChannel.join()
    .receive("ok", async resp => {
      console.log("Joined successfully", resp);

      peerConnection = createPeerConnection(localStream);

      if(resp.should_call){
        call();
      }
    })
    .receive("error", resp => { console.log("Unable to join", resp) });

  signalingChannel.on('ice_candidate', resp =>{
    console.log('new ice candidate', resp)

    let candidate = new RTCIceCandidate(resp.candidate);
    peerConnection
      .addIceCandidate(candidate)
      .catch(error => console.error('error while adding and ice candidate', error));
  });

  signalingChannel.on('sdp_offer', resp => {
    console.log('new sdp offer', resp)

    answerCall(resp.offer);
  });

  signalingChannel.on('sdp_answer', resp => {
    console.log('new sdp answer', resp);

    receiveRemote(resp.answer);
  })
}

function disconnect() {
  let remoteAudio = document.getElementById('remote-audio');

  unsetSource(remoteAudio);
  peerConnection.close();
  peerConnection = null;
  remoteStream = new MediaStream();
  pushMessage('leave',null);
  signalingChannel.leave();
}

const hooks = {
  ChatContainer: {
    async mounted() {
      await connect(this.el.dataset.roomId);
    },
    destroyed() {
      disconnect();
    }
  }
}



let liveSocket = new LiveSocket("/live", Socket, {hooks, params: {_csrf_token: csrfToken}})
let socket = new Socket("/socket", {params: {token: window.userToken}})

// Show progress bar on live navigation and form submits
topbar.config({barColors: {0: "#FF7438"}, barThickness: 5, shadowColor: "rgba(0, 0, 0, .3)"})
window.addEventListener("phx:page-loading-start", info => topbar.show())
window.addEventListener("phx:page-loading-stop", info => topbar.hide())

// connect if there are any LiveViews on the page
liveSocket.connect()
socket.connect()

// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)  // enabled for duration of browser session
// >> liveSocket.disableLatencySim()
window.liveSocket = liveSocket

