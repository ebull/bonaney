defmodule Bonaney.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      BonaneyWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Bonaney.PubSub},

      BonaneyWeb.Presence,

      {Bonaney.WaitingQueue, name: Bonaney.WaitingQueue},
      # Start the room registry
      {Registry, keys: :unique, name: Bonaney.RoomRegistry},
      # Start the room supervisor
      Bonaney.RoomSupervisor,
      # Start the Endpoint (http/https)
      BonaneyWeb.Endpoint,

      Bonaney.Stats,
      # Start a worker by calling: Bonaney.Worker.start_link(arg)
      # {Bonaney.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Bonaney.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    BonaneyWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
