defmodule Bonaney.Room do
  use GenServer, restart: :transient

  alias Bonaney.{Room, RoomRegistry}

  require Logger


  @timeout 180_000
  @chat_duration 60

  defstruct [:timer, remaining_time: @chat_duration, id: "", peer_count: 0]

  ### Client

  def start_link(room_id) do
    GenServer.start_link(__MODULE__, %Room{id: room_id}, [name: via_tuple(room_id), timeout: @timeout])
  end

  def begin_countdown(room_id) do
    GenServer.cast(via_tuple(room_id), :start_countdown)
  end

  def join(room_id) do
    GenServer.call(via_tuple(room_id), :join)
  end

  def leave(room_id) do
    GenServer.cast(via_tuple(room_id), :leave)
  end

  ### Server

  @impl true
  def init(room) do
    Logger.info("New room created, id: #{room.id}")

    {:ok, room, @timeout}
  end

  @impl true
  def handle_call(:join, _from, room) do
    new_room = %Room{room | peer_count: room.peer_count + 1}
    {:reply, {:ok, new_room}, new_room, @timeout}
  end

  @impl true
  def handle_call(:get_room, _from, room), do: {:reply, {:ok, room}, room, @timeout}

  @impl true
  def handle_cast(:leave, room) do
    Logger.info("A user has left room #{room.id}, stopping")
    Phoenix.PubSub.broadcast(Bonaney.PubSub, "expiration:" <> room.id, {:expiration, %{room: room, reason: :user_left}})
    Phoenix.PubSub.broadcast(Bonaney.PubSub, "expiration:all", {:expiration, %{room: room, reason: :user_left}})

    {:stop, :normal, room}
  end

  @impl true
  def handle_cast(:start_countdown, room) do
    {:noreply, %Room{room | timer: Process.send_after(self(), :tick, 1000)}, @timeout}
  end

  def handle_info(:tick, %Room{id: id, remaining_time: 0} = room) do
    Logger.info("Room #{id} is finished, stopping")
    Phoenix.PubSub.broadcast(Bonaney.PubSub, "expiration:" <> room.id, {:expiration, %{room: room, reason: :chat_end}})
    Phoenix.PubSub.broadcast(Bonaney.PubSub, "expiration:all", {:expiration, %{room: room, reason: :chat_end}})

    {:stop, :normal, room}
  end

  def handle_info(:tick, %Room{id: id, remaining_time: time} = room) do
    Phoenix.PubSub.broadcast(Bonaney.PubSub, "tick:" <> id, {:tick, %{time: time}})

    {:noreply, %Room{room | timer: Process.send_after(self(), :tick, 1000), remaining_time: time - 1}, @timeout}
  end


  @impl true
  def handle_info(:timeout, room) do
    Logger.info("Room #{room.id} timed out, stopping")

    Phoenix.PubSub.broadcast(Bonaney.PubSub, "expiration:" <> room.id, {:expiration, %{room: room, reason: :room_inactivity}})
    Phoenix.PubSub.broadcast(Bonaney.PubSub, "expiration:all", {:expiration, %{room: room, reason: :room_inactivity}})

    {:stop, :normal, room}
  end

  ### other stuff

  defp via_tuple(room_id) do
    {:via, Registry, {RoomRegistry, room_id}}
  end

  def exists?(room_id) do
    case Registry.lookup(Bonaney.RoomRegistry, room_id) do
      [] ->
        false
      [_] ->
        true
    end
  end

  def lookup(room_id) do
    case Registry.lookup(Bonaney.RoomRegistry, room_id) do
      [] ->
        {:empty, nil}
      [{pid, _}] ->
        GenServer.call(pid, :get_room)
    end
  end
end
