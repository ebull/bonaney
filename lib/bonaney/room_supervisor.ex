defmodule Bonaney.RoomSupervisor do
  use DynamicSupervisor

  alias Bonaney.Room

  def start_link(_arg) do
    DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def create_room(room_id) do
    DynamicSupervisor.start_child(Bonaney.RoomSupervisor, {Room, room_id})
  end

  @impl true
  def init(_arg) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end
end
