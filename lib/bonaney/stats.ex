defmodule Bonaney.Stats do
  use GenServer

  alias Bonaney.{Stats, RoomRegistry}

  require Logger

  defstruct [max_room_count: 0, room_count: 0]

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, %Stats{}, opts)
  end

  def get_stats() do
    GenServer.call(__MODULE__, :get_stats)
  end

  @impl true
  def init(stats) do
    Process.send_after(self(), :refresh, 20_000)

    {:ok, stats}
  end


  @impl true
  def handle_info(:refresh, stats) do

    Logger.debug("Refreshing the stats")

    new_stats =
      case Registry.count(RoomRegistry) do
        room_count when stats.max_room_count < room_count ->
          %Stats{room_count: room_count, max_room_count: room_count}
        room_count ->
          %Stats{room_count: room_count, max_room_count: stats.max_room_count}
      end

    Phoenix.PubSub.broadcast(Bonaney.PubSub, "stats:all", {:stats, new_stats})
    Process.send_after(self(), :refresh, 20_000)

    {:noreply, new_stats}
  end

end
