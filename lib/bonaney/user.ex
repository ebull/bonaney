defmodule Bonaney.User do
  defstruct id: ""

  alias Bonaney.User

  def create() do
    %User{id: UUID.uuid4()}
  end
end
