defmodule Bonaney.WaitingQueue do
  use GenServer

  require Logger

  def start_link(opts \\ []) do
    Logger.debug("Starting the waiting queue")

    {:ok, pid} = GenServer.start_link(__MODULE__, Qex.new(), opts)

    {:ok, pid}
  end

  def get_free_room() do
    GenServer.call(__MODULE__, :pop)
  end

  def add_free_room(room_id) do
    GenServer.cast(__MODULE__, {:add, room_id})
  end

  @impl true
  def init(queue) do
    Phoenix.PubSub.subscribe(Bonaney.PubSub, "expiration:all")

    {:ok, queue}
  end

  @impl true
  def handle_info({:expiration, %{room: room, reason: :room_inactivity}}, queue) do
    Logger.info("Removing room #{room.id} from the waiting queue")
    {:noreply, remove(queue, room.id)}
  end

  @impl true
  def handle_info({:expiration, %{room: room, reason: :user_left}}, queue) do
    Logger.info("Removing room #{room.id} from the waiting queue")
    {:noreply, remove(queue, room.id)}
  end

  @impl true
  def handle_info({:expiration, _}, socket), do: {:noreply, socket}

  @impl true
  def handle_call(:pop, _from, queue) do
    {res, new_queue} = Qex.pop(queue)
    {:reply, res, new_queue}
  end

  @impl true
  def handle_cast({:add, room_id}, queue) do
    new_queue = Qex.push(queue, room_id)

    {:noreply, new_queue}
  end

  defp remove(queue, room_id) do
    queue
      |> Enum.to_list()
      |> Enum.filter(fn(r) -> r != room_id end)
      |> Qex.new()
  end

end
