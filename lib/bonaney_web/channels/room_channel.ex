defmodule BonaneyWeb.RoomChannel do
  use BonaneyWeb, :channel

  alias Bonaney.Room

  require Logger

  @impl true
  def join("room:" <> room_id, _payload, socket) do
    if Room.exists?(room_id) do
      socket = assign(socket, :room_id, room_id)

      case Room.join(room_id) do
        {:ok, %Room{peer_count: 1}} ->
          {:ok, %{"should_call" => false}, socket}

        {:ok, %Room{peer_count: 2}} ->
          {:ok, %{"should_call" => true}, socket}

        {_, _} ->
          {:error, %{reason: "too many peers in this room"}}
      end
    else
      {:error, %{reason: "room doesn't exist"}}
    end
  end

  @impl true
  def handle_in("ice_candidate", data, socket) do
    Logger.debug("New ice candidate received")
    broadcast_from!(socket, "ice_candidate", data)
    {:noreply, socket}
  end

  @impl true
  def handle_in("sdp_offer", data, socket) do
    Logger.debug("New sdp offer received")
    broadcast_from!(socket, "sdp_offer", data)

    {:noreply, socket}
  end

  @impl true
  def handle_in("sdp_answer", data, socket) do
    Logger.debug("New sdp offer answer")

    Room.begin_countdown(socket.assigns.room_id)

    broadcast_from!(socket, "sdp_answer", data)

    {:noreply, socket}
  end

  @impl true
  def handle_in("leave", _data, socket) do
    Logger.debug("User leaving")
    room_id = socket.assigns.room_id

    if Room.exists?(room_id) do
      Room.leave(room_id)
    end

    {:noreply, socket}
  end
end
