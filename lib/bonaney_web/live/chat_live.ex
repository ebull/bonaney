defmodule BonaneyWeb.ChatLive do
  use BonaneyWeb, :live_view

  alias Bonaney.{Room, User}

  require Logger

  @impl true
  def mount(%{"room_id" => room_id}, _assigns, socket) do
    case Room.lookup(room_id) do
      {:ok, %Room{peer_count: peer_count} = room} when peer_count <= 1 ->
        join(room, socket)
      {_, _} ->
        {:ok,
          socket
          |> put_flash(:error, "Cette conversation n'existe pas")
          |> push_redirect(to: Routes.join_path(socket, :index))
        }
    end
  end

  defp join(room, socket) do
    user = User.create()

    Phoenix.PubSub.subscribe(Bonaney.PubSub, "tick:" <> room.id)
    Phoenix.PubSub.subscribe(Bonaney.PubSub, "expiration:" <> room.id)
    socket = socket
      |> assign(:room, room)
      |> assign(:user, user)
      |> assign(:remaining_time, 0)

    {:ok, socket}
  end

  @impl true
  def handle_event("leave", _from, socket) do
    {:noreply,
      socket
      |> push_redirect(to: Routes.join_path(socket, :index))}
  end

  @impl true
  def handle_info({:expiration, %{reason: :chat_end}}, socket) do
    {:noreply,
      socket
      |> put_flash(:info, "C'est fini! Tu peux relancer une conversation en appuyant sur le bouton ci-dessous.")
      |> push_redirect(to: Routes.join_path(socket, :index))}
  end

  @impl true
  def handle_info({:expiration, %{reason: :room_inactivity}}, socket) do
    {:noreply,
      socket
      |> put_flash(:info, "Il n'y a personne pour l'instant... Ré-essaie dans quelques minutes!")
      |> push_redirect(to: Routes.join_path(socket, :index))}
  end

  @impl true
  def handle_info({:expiration, %{reason: :user_left}}, socket) do
    {:noreply,
      socket
      |> put_flash(:info, "L'autre personne a quitté la conversation.")
      |> push_redirect(to: Routes.join_path(socket, :index))}
  end

  @impl true
  def handle_info({:tick, %{time: time}}, socket) do
    {:noreply,
      socket
      |> assign(:remaining_time, time)}
  end
end
