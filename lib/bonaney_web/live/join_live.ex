defmodule BonaneyWeb.JoinLive do
  use BonaneyWeb, :live_view

  alias Bonaney.{RoomSupervisor, WaitingQueue}

  require Logger

  @impl true
  def mount(_params, _assigns, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_event("join", _params, socket) do
    case WaitingQueue.get_free_room() do
      {:value, room_id} ->
        {:noreply,
          socket
          |> push_redirect(to: Routes.chat_path(socket, :chat, room_id))
        }
      :empty ->
        room_id = UUID.uuid4()
        RoomSupervisor.create_room(room_id)
        WaitingQueue.add_free_room(room_id)
        {:noreply,
          socket
          |> push_redirect(to: Routes.chat_path(socket, :chat, room_id))
        }
    end
  end
end
