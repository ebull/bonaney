defmodule BonaneyWeb.StatsLive do
  use BonaneyWeb, :live_view

  alias Bonaney.Stats

  @impl true
  def mount(_params, _assigns, socket) do
    Phoenix.PubSub.subscribe(Bonaney.PubSub, "stats:all")

    {:ok, socket |> assign(:stats, %Stats{}) }
  end

  @impl true
  def handle_info({:stats, stats}, socket), do: {:noreply, socket |> assign(:stats, stats) }


end
