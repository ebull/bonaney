defmodule BonaneyWeb.Presence do
  use Phoenix.Presence,
    otp_app: :bonaney,
    pubsub_server: Bonaney.PubSub
end
